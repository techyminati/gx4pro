# GX4_PRO-user 12 SP1A.210812.016 1680773996 release-keys
- manufacturer: gigasetgigaset
- platform: commonmt6789
- codename: GX4_PRO
- flavor: sys_mssi_64_ww_armv82-user
- release: 12
- id: SP1A.210812.016
- incremental: 1680773996
- tags: release-keys
- fingerprint: Gigaset/GX4_PRO_EEA/GX4_PRO:12/SP1A.210812.016/1680773996:user/release-keys
Gigaset/GX4_PRO_EEA/GX4_PRO:12/SP1A.210812.016/1680773996:user/release-keys
- is_ab: true
- brand: Gigaset
- branch: GX4_PRO-user-12-SP1A.210812.016-1680773996-release-keys
- repo: gigaset_gx4_pro_dump
